;;; simple combinations to convert or import external sources
;;;
;;; export LD_LIBRARY_PATH=/opt/dydra/lib:$LD_LIBRARY_PATH
;;; lein repl

(ns dydra-dataset.core
    (:gen-class)
    (:require
     [clojure.tools.cli :refer [cli]]
     [grafter.pipeline :refer [apply-pipeline]]
     [grafter.rdf :refer [s]]
     [grafter.rdf.io :refer [rdf-serializer]]
     [grafter.rdf.protocols :as pr]
     [grafter.rdf.repository :refer [->connection]]
     [grafter.rdf.templater :refer [graph]]
     [grafter.vocabularies.rdf :refer :all]
     [grafter.vocabularies.foaf :refer :all]
     [dydra-dataset.prefix :refer [base-id base-graph base-vocab base-data]]
     [dydra-dataset.pipeline :as pipeline :refer [read-dataset]]
     [dydra-dataset.iris-data :as iris]
     [dydra-dataset.plot-data :as plot]
     [dydra-dataset.transform :refer [->integer]]
     [dydra-dataset.tabular.arff ] ; include here as it is otherwise referenced just indirectly
     )
    (:import 
     org.openrdf.model.URI
     org.openrdf.model.Resource
     org.openrdf.model.Statement
     
     org.openrdf.model.impl.LiteralImpl
     org.openrdf.model.impl.StatementImpl
     org.openrdf.model.impl.URIImpl
     
     org.openrdf.repository.Repository
     org.openrdf.repository.manager.RepositoryManager
     org.openrdf.repository.http.HTTPRepository
     org.openrdf.repository.RepositoryConnection
     
     org.openrdf.repository.sail.SailRepository
     org.openrdf.sail.memory.MemoryStore
     
     org.openrdf.query.resultio.TupleQueryResultFormat
     
     com.dydra.ndk.sesame.DydraRepositoryManager
     com.dydra.ndk.sesame.DydraRepository
     weka.core.Instance
     [weka.core.converters ArffLoader CSVLoader]
     ))

(defn -main
  "Interpret command-line arguments and arrange the transformation.
   Based on the final argument, either import or write the transformed result."
  [& args]
  (let [[opts transformation description]
          (cli args ["-v" "--verbose" "Report progress" :flag true :default false]
                    ["-h" "--help" "Print invocation description" :flag true :default false])]
    (if (:help opts)
      (println description)
      (let [input (first transformation)
            output (last transformation)
            steps (butlast (rest transformation)) ]
	(letfn [(apply-steps [steps input-source]
                  (when (:verbose opts)
		    (println "Applying steps: '" steps "'"))
		  (if (seq steps)
		      (apply-pipeline (first steps)  (list (apply-steps (rest steps) input-source)))
		    (read-dataset input-source))) ]

	  (when (:verbose opts)
	    (println "Transforming: " input " -> " steps " -> " output))
	  (let [graph-data (apply-steps steps input)]
            (when (:verbose opts)
	      (println "loaded graph data: " graph-data))
	    (if (re-find #"\.(nq|rdf|nt|ttl)$" output)
		;; treat as external transform
		(let [serializer (rdf-serializer output)]
              (when (:verbose opts)
                (println "serializing to " output " through " serializer))
              (pr/add serializer graph-data))
            ;; otherwise, import to a dataset
            (let [manager (DydraRepositoryManager. )
                  repository (.getRepository manager output)]
              (.initialize repository)
		(pr/add repository graph-data)))))))))



(defn run-from-csv [steps input output]
  (letfn [(apply-steps [steps]
    	      (if (seq steps)
	        (apply-pipeline (first steps)  (list (apply-steps (rest steps))))
		(read-dataset input)))]
      (println "Transforming: (" steps " x " input  ") -> " output)
      (let [graph-data (apply-steps steps)]
        (if (re-find #"\.(nq|rdf|nt|ttl)$" output)
            ;; treat as external transform
            (let [serializer (rdf-serializer output)]
              (pr/add serializer graph-data))
            ;; otherwise, import to a dataset
            (let [manager (DydraRepositoryManager. )
                  repository (.getRepository manager output)]
              (.initialize repository)
		(pr/add repository graph-data))))))


;;; patches

;;; override the string conversion operator
(extend java.lang.String
 grafter.rdf.io/ISesameRDFConverter
 {:->sesame-rdf-type (fn [this]
                         (cond
                          (or (re-matches #"(http|https):.*" this)
                              (re-matches #"urn:uuid:[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" this))
                          (URIImpl. this)
                          (re-matches #"uuid:[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" this)
                          (URIImpl. (str "urn:" this))
                          (re-matches #"[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" this)
                          (URIImpl. (str "urn:uuid:" this))
                          :else
                          (LiteralImpl. this)))
 })

;;; (re-matches #"((urn:)?uuid:)?[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" "2943bdbd-450d-46f8-b4c7-5f2c0c079202")
;;; (re-matches #"((urn:)?uuid:)?[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" "uuid:2943bdbd-450d-46f8-b4c7-5f2c0c079202")
;;; (re-matches #"((urn:)?uuid:)?[\p{XDigit}]{8}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{4}-[\p{XDigit}]{12}" "urn:uuid:2943bdbd-450d-46f8-b4c7-5f2c0c079202")

;;; correct grafter meta to merge
(in-ns 'grafter.tabular.common)
(defn assoc-data-source-meta [output-ds data-source]
  "Adds metadata about where the dataset was loaded from to the object."
  (let [source-meta (cond
		     (#{String File URI URL} (class data-source))  {:grafter.tabular/data-source data-source}
		     (instance? incanter.core.Dataset data-source) (meta data-source)
		     :else {:grafter.tabular/data-source :datasource-unknown}) ]
    (with-meta output-ds (merge (meta output-ds) source-meta))))


;;; (import com.dydra.ndk.sesame.DydraRepositoryManager)
;;; (import com.dydra.ndk.sesame.DydraRepository)
;;; (def manager (DydraRepositoryManager. ))
;;; (def manager (com.dydra.ndk.sesame.DydraRepositoryManager. ))
;;; (def repository (.getRepository manager "bendiken/test"))
;;; the repository is also available directly as a concrete class
;;; (def repository (com.dydra.ndk.sesame.DydraRepository. "marti/test"))

;;; the interface is apparent through introspection
;;; (-> com.dydra.ndk.sesame.DydraRepository .getClass .getDeclaredMethods pprint)

;;; (.initialize repository)
;;; (def connection (.getConnection repository))
;;; (def query (.prepareTupleQuery connection org.openrdf.query.QueryLanguage/SPARQL "SELECT * WHERE {?s ?p ?o}"))
;;; (def results (.evaluate query))
;;; (.hasNext results) (.next results) (.close results)
;;; (grafter.rdf.repository/evaluate query)

