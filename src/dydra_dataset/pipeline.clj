(ns dydra-dataset.pipeline
    (:require
     [incanter.core :refer [col-names]]
     [grafter.rdf.io :refer [rdf-serializer literal-datatype->type]]
     [grafter.rdf.protocols :as pr]
     [grafter.pipeline :refer [apply-pipeline]]
     [grafter.tabular :refer [defpipe defgraft column-names columns rows
                              derive-column mapc swap drop-rows
                              make-dataset
                              move-first-row-to-header _ graph-fn melt
                              test-dataset]]
     [dydra-dataset.prefix :refer [base-id base-graph base-vocab base-data]]
     [dydra-dataset.transform :refer [->integer]]
     [dydra-dataset.vocabularies.marti :refer :all]
     [clj-ml.data ]
     [clj-ml.io ] )
    (:import
      java.net.URI
      com.dydra.ndk.sesame.DydraRepositoryManager
      com.dydra.ndk.sesame.DydraRepository
      org.openrdf.model.impl.StatementImpl
      org.openrdf.query.BindingSet
      org.openrdf.query.parser.ParsedBooleanQuery
      org.openrdf.query.parser.ParsedGraphQuery
      org.openrdf.query.parser.ParsedTupleQuery
      org.openrdf.repository.sail.SailBooleanQuery
      org.openrdf.repository.sail.SailGraphQuery
      org.openrdf.repository.sail.SailTupleQuery
     ))

;;; extend grafter's pipline implementation with elements:
;;; - canonicalize an anonymous csv-based dataset into one with the correct header and symbol keys
;;; - converters between incanter and weka datasets
;;; - short-hand operators to read specific sources as a dataset or as a graph
;;; - a shadow for read dataset which canonicalizes the dataset
;;; - sparql result decoding to produce either a tuple- or a graph-based dataset with value conversion
;;; - RepositoryQuery record definition to combine a parsed query and a repository as a dataset source

(defn make-query-discriminator [repository query-string options] (class repository))
(defmulti make-query make-query-discriminator)

(defn bind-query-discriminator [query connection options] (class query))
(defmulti bind-query bind-query-discriminator)

(defn query-repository-as-dataset-discriminator [repository query options] (class query))
(defmulti query-repository-as-dataset query-repository-as-dataset-discriminator)


;;; pipe segments

(defpipe csv-to-tabular-dataset
  "accept a dataset which originated from a csv file and 'canonicalize' it
   by deriving a replacement header from the first row."
  [dataset]
  (let [column-names (col-names dataset)
        [header-data & actual-data] (:rows dataset)
        meta-data (meta dataset)
        actual-data-dataset (make-dataset actual-data)
        column-symbols (map (fn [name] (symbol (get header-data name))) column-names)]
    (with-meta (col-names actual-data-dataset column-symbols)
	       (merge meta-data {:column-symbols column-symbols}))))


(defpipe tabular-to-ml-dataset
  "Pipe segment to convert a grafter/incanter dataset into an ml dataset"
  [dataset]
  ;; this involves:
  ;; creating an attribute map from the original dataset columns names
  ;; constructung a data field in which the values are reordered to agree with the column order
  ;; and repackaged as a vector of vectors
  (assert (grafter.tabular.common/dataset? dataset) "The dataset argument must be a tabular dataset.")
  (let [columns (:column-names dataset)
        name (or (:name dataset) "unknown")
        data (into [] (map (fn [row] (into [] (map (fn [col] (row col)) columns))) (:rows dataset)))
        attributes (into [] (map (fn [col data] (if (string? data) (hash-map col nil) col))
	                         columns
                                 (first data)))
        ml-dataset (clj-ml.data/make-dataset name attributes data) ]
    ;; construct the ml dataset with the rearranged components
    (if (instance? clojure.lang.IObj ml-dataset)
      (with-meta ml-dataset (meta dataset))
      ml-dataset)))


(defpipe ml-to-tabular-dataset
  "Pipe segment which accepts an ml dataset and converts it into a grafter/incanter form"
  [dataset]
  (assert (clj-ml.data/is-dataset? dataset) "The dataset provided must be an ml dataset.")
  (let [attributes (clj-ml.data/dataset-attributes dataset)
        columns (map (fn [attr] (symbol (clj-ml.data/attr-name attr))) attributes)
        meta-data (if (instance? clojure.lang.IObj dataset) (meta dataset) {})
        data (map (fn [row] (zipmap columns
	                            (map (fn [value] (if (or (symbol? value) (keyword? value)) (name value) value)) row)))
                  (map clj-ml.data/instance-to-vector (clj-ml.data/dataset-seq dataset))) ]
    (with-meta (grafter.tabular/make-dataset data columns)
	       (merge meta-data {:column-symbols columns}))))



;;; dataset operators, bothh format-specific and a version which distinguishes post-processing
;;; based on the intermediate result

(defpipe read-tabular-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (csv-to-tabular-dataset (grafter.tabular/read-dataset data-file)))

(defpipe read-ml-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (ml-to-tabular-dataset (clj-ml.io/load-instances :arff data-file)))

(defpipe read-dataset
  "Wrap the base reader with logic to automatically promote the first row of a csv to the header"
  [source]
  (let [dataset (grafter.tabular/read-dataset source)]
    (if (:column-symbols (meta dataset))
      dataset
      (csv-to-tabular-dataset dataset))))

(defpipe write-dataset
  "Consolidate read/write api extensions in one namespace."
  [destination dataset & options]
  (apply grafter.tabular/write-dataset destination dataset options))

(defpipe ml-to-tabular-dataset
  "Pipe segment which accepts an ml dataset and converts it into a grafter/incanter form"
  [dataset]
  (assert (clj-ml.data/is-dataset? dataset) "The dataset provided must be an ml dataset.")
  (let [attributes (clj-ml.data/dataset-attributes dataset)
        columns (map (fn [attr] (symbol (clj-ml.data/attr-name attr))) attributes)
        meta-data (if (instance? clojure.lang.IObj dataset) (meta dataset) {})
        data (map (fn [row] (zipmap columns
	                            (map (fn [value] (if (or (symbol? value) (keyword? value)) (name value) value)) row)))
                  (map clj-ml.data/instance-to-vector (clj-ml.data/dataset-seq dataset))) ]
    (with-meta (grafter.tabular/make-dataset data columns)
	       (merge meta-data {:column-symbols columns}))))



;;; dataset operators, bothh format-specific and a version which distinguishes post-processing
;;; based on the intermediate result

(defpipe read-tabular-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (csv-to-tabular-dataset (grafter.tabular/read-dataset data-file)))

(defpipe read-ml-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (ml-to-tabular-dataset (clj-ml.io/load-instances :arff data-file)))

(defpipe read-dataset
  "Wrap the base reader with logic to automatically promote the first row of a csv to the header"
  [source]
  (let [dataset (grafter.tabular/read-dataset source)]
    (if (:column-symbols (meta dataset))
      dataset
      (csv-to-tabular-dataset dataset))))

;;; dataset operators, bothh format-specific and a version which distinguishes post-processing
;;; based on the intermediate result

(defpipe read-tabular-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (csv-to-tabular-dataset (grafter.tabular/read-dataset data-file)))

(defpipe read-ml-dataset
  "Pipe segment to read tabular data into a new canonical dataset."
  [data-file]
  (ml-to-tabular-dataset (clj-ml.io/load-instances :arff data-file)))

(defpipe read-dataset
  "Wrap the base reader with logic to automatically promote the first row of a csv to the header"
  [source]
  (let [dataset (grafter.tabular/read-dataset source)]
    (if (:column-symbols (meta dataset))
      dataset
      (csv-to-tabular-dataset dataset))))


;;; reimplement the sparql results -> dataset transformer from grafter/repository.clj
;;; to perform coercion to incanter cell values consistent with the csv sparql result conventions
;;; (see http://www.w3.org/TR/2013/REC-sparql11-results-csv-tsv-20130321/)

(defmulti term-value class)
(defmethod term-value org.openrdf.model.Literal [literal] (literal-datatype->type literal))
(defmethod term-value org.openrdf.model.URI [uri] (.toString uri))
(defmethod term-value org.openrdf.model.BNode [node] (clojure.string/join ["_:" (.getID node)]))

(defn query-bindings->map [^org.openrdf.query.BindingSet qbs]
  (let [boundvars (.getBindingNames qbs)]
    (->> boundvars
         (mapcat (fn [k]
                   [(symbol k) (-> qbs (.getBinding k) .getValue term-value)]))
         (apply hash-map))))

(defn tuple-results-seq [results]
  (if (.hasNext results)
      (let [current-result (try
			    (query-bindings->map (.next results))
			    (catch Exception e
				   (.close results)
				   (throw e)))]
	(lazy-cat [current-result] (tuple-results-seq results)))
      (.close results)))

(defn statement->map [^org.openrdf.model.impl.StatementImpl statement]
  (zipmap (map symbol ["s" "p" "o"])
           [ (term-value (grafter.rdf/subject statement))
	     (term-value (grafter.rdf/predicate statement))
	     (term-value (grafter.rdf/object statement)) ]))

(defn graph-results-seq [results]
  (if (.hasNext results)
      (let [current-result (try
			    (statement->map (.next results))
			    (catch Exception e
				   (.close results)
				   (throw e)))]
	(lazy-cat [current-result] (graph-results-seq results)))
      (.close results)))

(defn iri-key [iri]
  (let [url (java.net.URI. iri)
        ref (.getFragment url)
        path (.getPath url)]
    (or ref
	(when (and path (pos? (.length path))) (last (clojure.string/split path #"/")))
	iri)))



;;; implement query execution and conversion for tuple and graph queries

(defmethod query-repository-as-dataset org.openrdf.query.parser.ParsedTupleQuery
  [repository query options]
  (with-open [connection (.getConnection repository )]
    (let [bound-query (bind-query query connection options)
          results (.evaluate bound-query)
          data (tuple-results-seq results)
          names (.getBindingNames results)
          symbols (map symbol names)
          dataset (with-meta (make-dataset data symbols)
	                     {:column-symbols symbols :query-string (.getSourceString query)}) ]
       ; (println data)
       dataset)))


(defmethod query-repository-as-dataset org.openrdf.query.parser.ParsedGraphQuery
  [repository query options]
  (with-open [connection (.getConnection repository )]
    (let [bound-query (bind-query query connection options)
          results (.evaluate bound-query)
          subject-symbol :s]
      (letfn [(add-property [results resource-map header]
                (if (.hasNext results)
                  (let [statement (.next results)
		        subject-value (term-value (grafter.rdf/subject statement))
		        predicate-value (term-value (grafter.rdf/predicate statement))
		        object-value (term-value (grafter.rdf/object statement))
		        subject-map (get resource-map subject-value)
		        [predicate-key new-header] (predicate-key predicate-value header)]
                     (add-property results
 				  (assoc resource-map
					 subject-value
					 (if subject-map
					     (assoc subject-map predicate-key object-value)
					   (hash-map subject-symbol subject-value predicate-key object-value)))
				  new-header))
		[resource-map header]))
             (predicate-key [predicate header-map]
               (let [key (get header-map predicate)]
		 (if key
		     [key header-map]
		     (let [key (iri-key predicate)]
		       [key (assoc header-map predicate key)])))) ]
	(try (let [[data header-map] (add-property results { } {subject-symbol subject-symbol})
		  header (reverse (vals header-map)) ]
               (with-meta (make-dataset (vals data) header)
			  {:column-symbols header :query-string (.getSourceString query)} ))
	     (finally (.close results)))))))


;;; encapsulate a query x repository x options combination as a dataset source

(defmulti testf (fn [arg1 arg2] (class arg1)))

(defrecord RepositoryQuery [repository query options] )

(defmethod testf java.lang.String [arg1 arg2] (list arg1 arg2))
(defmethod testf RepositoryQuery [arg1 arg2] (list arg1 :repo))
(defmethod testf :default [arg1 arg2] :default)


(defmethod grafter.tabular.common/read-dataset-source RepositoryQuery
  [repository-query options]
  "Specialize the graphter origin query with a method to generate a dataset
   from query results."
  (query-repository-as-dataset (:repository repository-query)
			    (:query repository-query)
			    (merge (:options repository-query) options)))


(defmethod make-query java.lang.String
  [repository-name query-string options]
  "Construct and prepare a new tuple or graph query given the repository name and the query string.
   Ensure that the connection is marked read-only, as is the case for a query, in order
   to permit parallel operations."
  (let [manager (DydraRepositoryManager. )
        repository (.getRepository manager repository-name) ]
    (.initialize repository)
    (make-query repository query-string options)))

(defmethod make-query org.openrdf.repository.Repository [repository query-string options]
  (->RepositoryQuery repository
		     (org.openrdf.query.parser.QueryParserUtil/parseQuery
                       org.openrdf.query.QueryLanguage/SPARQL
		       query-string
                       nil)
		     options))



(defpipe ml-to-tabular-dataset
  "Pipe segment which accepts an ml dataset and converts it into a grafter/incanter form"
  [dataset]
  (assert (clj-ml.data/is-dataset? dataset) "The dataset provided must be an ml dataset.")
  (let [attributes (clj-ml.data/dataset-attributes dataset)
        columns (map (fn [attr] (symbol (clj-ml.data/attr-name attr))) attributes)
        meta-data (if (instance? clojure.lang.IObj dataset) (meta dataset) {})
        data (map (fn [row] (zipmap columns
	                            (map (fn [value] (if (or (symbol? value) (keyword? value)) (name value) value)) row)))
                  (map clj-ml.data/instance-to-vector (clj-ml.data/dataset-seq dataset))) ]
    (with-meta (grafter.tabular/make-dataset data columns)
	       (merge meta-data {:column-symbols columns}))))




;; (defprotocol QueryBinder
;;   (bind-query-to-connection [context query]))

;; (extend org.openrdf.repository.sail.SailRepositoryConnection
;;   QueryBinder
;;   {:bind-query-to-connection (fn [^org.openrdf.repository.sail.SailRepositoryConnection connection ^org.openrdf.query.parser.ParsedQuery query]
;; 				 (cond (instance? ParsedBooleanQuery query)
;; 				       (new SailBooleanQuery query connection)
;; 				       (instance? ParsedGraphQuery query)
;; 				       (SailGraphQuery. query connection)
;; 				       (instance? ParsedTupleQuery query)
;; 				       (SailTupleQuery. query connection))) })
;; still:
;; CompilerException java.lang.IllegalArgumentException: No matching ctor found for class org.openrdf.repository.sail.SailBooleanQuery, compiling:(dydra_csv/pipeline.clj:237:12) 
				   

(defmethod bind-query org.openrdf.query.parser.ParsedGraphQuery [query connection options]
  (.prepareQuery connection org.openrdf.query.QueryLanguage/SPARQL (.getSourceString query)))

(defmethod bind-query org.openrdf.query.parser.ParsedBooleanQuery [query connection options]
  (.prepareQuery connection org.openrdf.query.QueryLanguage/SPARQL (.getSourceString query)))

(defmethod bind-query org.openrdf.query.parser.ParsedTupleQuery [query connection options]
  (let [prepared  (.prepareQuery connection  org.openrdf.query.QueryLanguage/SPARQL (.getSourceString query))]
    prepared))





;;; (dydra-dataset.pipeline/read-dataset "data/plots-one.csv")
;;; (dydra-dataset.pipeline/read-dataset "data/iris.arff")

;;; api operator to interpret a combination

(defn apply-import-pipeline [steps input output & {:keys [reader] :or {reader "read-dataset"}}]
  "Given a pipeline and an import source, read an initial dataset from the source
   and pass it through the pipeline"
  (let [reader-function (resolve (symbol reader))]
    (letfn [(apply-steps [steps input-source]
              (if (seq steps)
                (apply-pipeline (first steps)  (list (apply-steps (rest steps) input-source)))
                (reader-function input-source))) ]
       ;(println "Transforming: (" steps " x " input  ") -> " output)
       (let [graph-data (apply-steps steps input)]
         (if (re-find #"\.(nq|rdf|nt|ttl)$" output)
           ;; treat as external transform
           (let [serializer (rdf-serializer output)]
             (pr/add serializer graph-data))
           ;; otherwise, import to a dataset
           (let [manager (DydraRepositoryManager. )
                 repository (.getRepository manager output)]
             (.initialize repository)
             ;(println "Graph data: " (doall graph-data) " Class: " (.getClass graph-data))
             (pr/add repository graph-data)))))))
