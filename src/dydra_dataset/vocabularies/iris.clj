(ns dydra-dataset.vocabularies.iris (:require [grafter.rdf :refer [prefixer]]))

(def ->iris-resource (prefixer "http://example.org/iris/"))
(def ->iris-property (prefixer "http://example.org/iris#"))

(def iris:Iris (->iris-resource "Iris"))
(def iris:sepalLength (->iris-property "sepallength"))
(def iris:sepalWidth (->iris-property "sepalwidth"))
(def iris:petalLength (->iris-property "petallength"))
(def iris:petalWidth (->iris-property "petalwidth"))
(def iris:class (->iris-property "class"))

