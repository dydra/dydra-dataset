(ns dydra-dataset.vocabularies.marti (:require [grafter.rdf :refer [prefixer]]))

(def ->marti-resource (prefixer "http://agri-esprit.com/marti/"))
(def ->marti-property (prefixer "http://agri-esprit.com/marti#"))

(def marti:crop_plot (->marti-property "crop_plot"))
(def marti:field (->marti-property "field"))
(def marti:Field (->marti-resource "Field"))
(def marti:end_date (->marti-property "end_date"))
(def marti:id (->marti-property "id"))
(def marti:plot (->marti-property "plot"))
(def marti:start_date (->marti-property "start_date"))

