(ns dydra-dataset.plot-data
    (:require
     [grafter.tabular :refer [defpipe defgraft graph-fn]]
     [grafter.rdf :refer [s]]
     [grafter.rdf.protocols :refer [->Quad]]
     [grafter.rdf.templater :refer [graph]]
     [grafter.vocabularies.rdf :refer :all]
     [dydra-dataset.pipeline :refer [read-dataset]]
     [dydra-dataset.prefix :refer [base-id base-graph base-vocab base-data]]
     [dydra-dataset.vocabularies.marti :refer :all]
     [dydra-dataset.transform :refer [->integer]]))


(def make-graph
    (graph-fn [[end_date field legal_entity name start_date urn]]
            (graph (base-graph "crop_fields")
                   [field
                    [rdf:a marti:Field]
                    [marti:start_date start_date]
                    [marti:end_date end_date]
                    [rdfs:label name]
		    [marti:plot urn]
		    ])))


(defgraft read-graph
  "Pipeline to convert the tabular crop plot data into a graph."
  read-dataset make-graph)

