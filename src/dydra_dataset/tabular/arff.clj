(ns dydra-dataset.tabular.arff
  {:no-doc true}
  (:require [grafter.tabular.common :as tab]
            [dydra-dataset.pipeline ]
            [clj-ml.data ]
            [clj-ml.io ] )
  )

(defmethod tab/read-dataset* :arff
  [source opts]
  (let [ml-dataset (clj-ml.io/load-instances :arff source)]
    (let [tabular-dataset (dydra-dataset.pipeline/ml-to-tabular-dataset ml-dataset)]
      tabular-dataset)))

(defmethod tab/read-datasets* :arff
  [source opts]
  (when-let [ds (tab/mapply tab/read-dataset source opts)]
    [{"arff" ds}]))

(defmethod tab/write-dataset* :arff [destination dataset {:keys [format] :as opts}]
  (clj-ml.io/save-instances "arff" destination (dydra-dataset.pipeline/tabular-to-ml-dataset dataset)))

