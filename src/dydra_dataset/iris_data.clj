(ns dydra-dataset.iris-data
    (:require
     [grafter.tabular :refer [defpipe defgraft graph-fn]]
     [grafter.rdf :refer [s]]
     [grafter.rdf.protocols :refer [->Quad]]
     [grafter.rdf.templater :refer [graph]]
     [grafter.vocabularies.rdf :refer :all]
     [dydra-dataset.pipeline :refer [read-dataset]]
     [dydra-dataset.prefix :refer [base-id base-graph base-vocab base-data]]
     [dydra-dataset.vocabularies.iris :refer :all]
     [dydra-dataset.transform :refer [->integer]]))




(def make-graph
  (graph-fn [[sepallength sepalwidth petallength petalwidth class]]
    (let [id (str "urn:uuid:" (java.util.UUID/randomUUID))]
      (graph (base-graph "iris")
	[id
	[rdf:a iris:Iris]
	[iris:sepalLength sepallength]
	[iris:sepalWidth sepalwidth]
	[iris:petalLength petallength]
        [iris:petalWidth petalwidth]
	[iris:class class]
	]))))

(defgraft read-graph
  "Pipeline to convert the tabular iris data into a graph."
  read-dataset make-graph)

