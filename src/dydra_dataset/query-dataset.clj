(nd dydra-dataset.rdf-dataset
    (:require 
     [grafter.tabular :refer [defpipe defgraft column-names columns rows
                              derive-column mapc swap drop-rows
                              make-dataset]]
			      ))


;;; (grafter.rdf.repository/query 

(comment
 (def repository (com.dydra.ndk.sesame.DydraRepository. "pgsql/weather"))
 (.initialize repository)
 (with-open [conn (grafter.rdf.repository/->connection repository)]
     (doseq [res (grafter.rdf.repository/query conn "SELECT * WHERE { ?s ?p ?o .}")]
        (println res)))

 (def connection (.getConnection repository))
 (def query (.prepareTupleQuery connection org.openrdf.query.QueryLanguage/SPARQL "SELECT * WHERE {?s ?p ?o}"))
 (def results (.evaluate query))
 )

;;; (def manager (DydraRepositoryManager. ))                                                                                                     
;;; (def manager (com.dydra.ndk.sesame.DydraRepositoryManager. ))                                                                                
;;; (def repository (.getRepository manager "bendiken/test"))                                                                                    
;;; the repository is also available directly as a concrete class                                                                                
;;; (def repository (com.dydra.ndk.sesame.DydraRepository. "bendiken/test"))                                                                     
;;; the interface is apparent through introspection                                                                                              
;;; (-> com.dydra.ndk.sesame.DydraRepository .getClass .getDeclaredMethods pprint)                                                               
;;; (.initialize repository)                                                                                                                     
;;; (def connection (.getConnection repository))                                                                                                 
;;; (def query (.prepareTupleQuery connection org.openrdf.query.QueryLanguage/SPARQL "SELECT * WHERE {?s ?p ?o}"))                               
;;; (def results (.evaluate query))                                                                                                              

