# dydra-dataset

This library augments [Grafter](https://github.com/Swirrl/grafter) with
components to facilitate interchange between external sources, a
[Dydra](http://dydra.com)
[RDF](https://github.com/JoshData/rdfabout/blob/gh-pages/intro-to-rdf.md)
repository and a [Weka ML](https://en.wikipedia.org/wiki/Weka_(machine_learning))
environment.

    (require '[dydra-dataset.pipeline :as pipeline])
    (require '[dydra-dataset.iris-data :as iris])
    (require '[dydra-dataset.plot-data :as plot])

The project file incorporates our native Sesame SAIL implementation, but it
should be possible to substitute any other SAIL implementation. In order to
have our sail implementation available to the Java run-time, please ensure that the
library file location, /opt/dydra/lib, is present on the LD_LIBRARY_PATH, for example, by
extending the einvironment in the account's .profile :

    export LD_LIBRARY_PATH="/usr/local/lib:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/opt/dydra/lib" 




## Using data sources

Files can be loaded and held as a dataset

    (pipeline/read-dataset "./data/plots-one.csv")

or as a graph

    (plot/read-graph "./data/plots-one.csv")

or imported directly into a repository

    (pipeline/apply-import-pipeline ["dydra-dataset.plot-data/make-graph"]
                                    "data/plots-all.csv"
                                    "marti/test")

In this case, the pipeline applies just the one step to transform the tabular
data into a graph for import. In general, however a
[pipeline](http://grafter.org/getting-started/030-understanding-pipes.html)
can apply various transformation steps to the data preliminary to importing
it into the repository.

The data source permits alternative encodings. In addition to single
spreadsheets encoded as "csv" files, the import logic recognizes "xls" form
Excel documents and "arff" documents which are intended for the Weka machine
learning environment.

In order to use [Incanter](http://incanter.org) to work with ARFF-encoded
Weka files and to import them into a repository, they can be converted
automatically into tabular form when read. For example, to read an ARFF file:

    (pipeline/read-dataset "./data/iris.arff")

This decodes the "arff" format and transforms it into a tabular
representation for use with Incanter or for import into a store.
Each table form requires its own logic, to map columns into predicates and
transform the rows into triples. For example,

    (iris/read-graph "./data/iris.arff")

This logic can be incorporated into a pipeline to import the ARFF into a
repository. For example, to import an ARFF file into a repository, specify
the keyword argument

    (pipeline/apply-import-pipeline ["dydra-dataset.iris-data/make-graph"]
                                    "data/iris.arff"
                                    "marti/test")


## Preparing data for the Weka ML environment

Tabular data is made available for use in the ML environment by converting
from the Incanter dataset. For example:

    (pipeline/tabular-to-ml-dataset (pipeline/read-dataset "./data/plots-one.csv"))

In order to save the data in ARFF format, write the dataset to a file

    (pipeline/write-dataset
     "plots.arff"
     (pipeline/tabular-to-ml-dataset (pipeline/read-dataset "./data/plots-one.csv")))


or direct a pipeline at an `arff` destination 


## Using RDF repositories

Data from an RDF repository can also serve as the source for a tabular
dataset. In this case, the repository is combined with a projection query,
which generates the tabular data. For example, the repository can be
specified by name in the form of a string

    (pipeline/read-dataset
      (pipeline/make-query "marti/plots"
                           "select  ?field ?start_date ?plot
                            where { ?field <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://agri-esprit.com/marti/Field> .
                                    ?field <http://agri-esprit.com/marti#start_date> ?start_date .
                                    ?field <http://agri-esprit.com/marti#plot> ?plot }"
                          {}))

or it can be instantiated for re-use and provided as the instance

    (def weather-repository (.getRepository  (DydraRepositoryManager. ) "pgsql/weather"))
    (.initialize weather-repository)
    (pipeline/read-dataset
      (pipeline/make-query weather-repository
                           "select  ?id ?windSpeed ?tmpAirAvg ?insideHumidity
                            where { ?id <urn:sensor:WindSpeed> ?windSpeed .
                                    ?id <urn:sensor:TmpAirAvg> ?tmpAirAvg .
                                    ?id <urn:sensor:InsideHumidity> ?insideHumidity .
                                  }"
		           {}))

A dataset can also be constructed from a graph query in order to collect all
properties for each known resource

    (pipeline/read-dataset
      (pipeline/make-query "pgsql/weather"
                           "describe ?s
                            where {?s ?p ?o}"
		           {}))


---
nb. For information about Leiningen, please refer to [leiningen.org](http://leiningen.org)