(defproject dydra-dataset "0.1.0-SNAPSHOT"
  :description "dydra tools to work with csv/arff/repository datasets"
  :url "http://dydra.com"
  :license {:name "Unlicense"
            :url "http://unlicense.org"}
  ;; locate the dydra java ndk in a local maven repository
  :repositories [["dydrarepo" "file:///opt/dydra/share/maven"]
                 ]
  :dependencies [;;[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojure "1.5.1"]
                 [org.clojure/tools.cli "0.2.4"]
                 [grafter "0.5.0"]
                 [grafter/vocabularies "0.1.2"]
                 [org.slf4j/slf4j-jdk14 "1.7.5"]
                 [org.openrdf.sesame "2.7.3"]
                 [com.dydra/dydra-ndk "0.0.1"]
                 [nz.ac.waikato.cms.weka/weka-dev "3.7.11"]
          	 [cc.artifice/clj-ml "0.6.1"]
		 ]
  :repl-options {:init-ns dydra-dataset.core }
  :jvm-opts ^:replace ["-server"
                       ;;"-XX:+AggressiveOpts"
                       ;;"-XX:+UseFastAccessorMethods"
                       ;;"-XX:+UseCompressedOops"
                       ;;"-Xmx4g"
                       ]

  :plugins [[lein-grafter "0.5.0"]]
  :min-lein-version "2.5.1"

  :main dydra-dataset.core
  :aot :all

  :javac-options ["-Xlint:deprecation" "-Xlint:unchecked"]
  )
